/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.livraria.entity;

import br.com.senac.livraria.banco.GeneroDAO;
import java.util.List;

/**
 *
 * @author MegaxHost
 */
public class Teste {
    
    public static void main(String... a){
        
        Genero genero = new Genero();
        genero.setDescricao("Drama");
        
        GeneroDAO dao = new GeneroDAO() ; 
        
        dao.save(genero);
        
        List<Genero> lista =dao.findAll();
        
        for(Genero g : lista){
            System.out.println(g.getDescricao());
        }
        
        
        
    }
    
}
